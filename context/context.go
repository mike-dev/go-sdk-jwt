package context

import (
	"context"

	"github.com/openzipkin/zipkin-go"
	stdzipkin "github.com/openzipkin/zipkin-go/propagation/b3"
)

// KV is a container for the context keys and values.
type KV map[string]string

// Apply applies key and values on provided setter.
func (kv KV) Apply(setter interface{ Set(key, value string) }) {
	for key, value := range kv {
		setter.Set(key, value)
	}
}

// Slice returns keys and values in a slice representation.
func (kv KV) Slice() []string {
	sl := make([]string, 0, len(kv)*2)
	for key, value := range kv {
		sl = append(sl, key, value)
	}
	return sl
}

// ExtractZipkinSpan extracts zipkin span from a context and returns KV, otherwise nil.
func ExtractZipkinSpan(ctx context.Context) KV {
	span := zipkin.SpanFromContext(ctx)
	if span == nil {
		return nil
	}

	sc := span.Context()

	kv := KV{}

	// x-b3-flags is used in debug mode
	kv[stdzipkin.Flags] = "0"

	if sc.Debug {
		kv[stdzipkin.Flags] = "0"
	} else if sc.Sampled != nil {
		// Debug is encoded as X-B3-Flags: 1. Since Debug implies Sampled,
		// so don't also send "X-B3-Sampled: 1".
		if *sc.Sampled {
			kv[stdzipkin.Sampled] = "1"
		} else {
			kv[stdzipkin.Sampled] = "0"
		}
	}

	// from parent span
	if !sc.TraceID.Empty() && sc.ID > 0 {
		kv[stdzipkin.TraceID] = sc.TraceID.String()
		kv[stdzipkin.SpanID] = sc.ID.String()

		if sc.ParentID != nil {
			kv[stdzipkin.ParentSpanID] = sc.ParentID.String()
		}
	}
	return kv
}
