// Package context provides context utilities helpers and constants.
package context

const (
	// CorrelationID is a key used for transport headers and context.
	CorrelationID = "correlation-id"
	// JWT is a key used for context.
	JWT = "jwt-token"
	// ServiceJWT is a key used to access service token within context.
	ServiceJWT = "service-jwt-token"
	// Timeout is the timeout header.
	Timeout = "timeout"
	// ItemsCount is a key used in grpc to send number of products which client should expect from server.
	ItemsCount = "items-count"
	// WorkflowStep is a key to denote the workflow step.
	WorkflowStep = "workflow-step"
)
