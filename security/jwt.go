package security

import (
	"context"
	"fmt"
	"time"

	keys "bitbucket.org/mike-dev/go-sdk-jwt/context"
	"bitbucket.org/mike-dev/go-sdk-jwt/errors"
	"github.com/dgrijalva/jwt-go"
)

const (
	// JwtCompanyIDField is a company id field.
	JwtCompanyIDField = "company_uuid"
	// JwtCreatedByField is a created by field.
	JwtCreatedByField = "uid_uuid"
	// JwtUserRoleField is user role field.
	JwtUserRoleField = "user_role"
)

const (
	// UserRoleAdmin is a admin role.
	UserRoleAdmin = "ADMIN"
	// UserRoleManager is a manager role.
	UserRoleManager = "MANAGER"
)

const bearerPrefix = "bearer"

// JWTSecured is implemented by structures which contain JWT field for later usage.
type JWTSecured interface {
	Claims(ctx context.Context, secret string, method jwt.SigningMethod) (jwt.MapClaims, error)
	SetToken(string)
	Token() string
}

// JWTAuthorizer is implemented by object which secured with token
// and can be used for additional token claims checks.
type JWTAuthorizer interface {
	Authorize(claims jwt.MapClaims) error
	JWTSecured
}

// PostAuthorizer is implemented by object which need to
// do response security checks.
type PostAuthorizer interface {
	PostAuthorize(claims jwt.MapClaims) error
}

// JWTConfig is implemented by structures which contain field for extend JWT life time.
type JWTConfig struct {
	Secret     string
	ExtendTime time.Duration
	Method     jwt.SigningMethod
}

type simpleMembershipMetadata struct {
	companyID string
	userID    string
}

func (m simpleMembershipMetadata) CompanyID() string {
	return m.companyID
}

func (m simpleMembershipMetadata) UserID() string {
	return m.userID
}

type simpleJWTSecured struct {
	jwtToken string
}

// SetToken apply provided token to the structure.
func (r *simpleJWTSecured) SetToken(token string) {
	r.jwtToken = token
}

// Token returns token string value.
func (r simpleJWTSecured) Token() string {
	return r.jwtToken
}

// Claims validates default token fields (exp, iat and etc.), signing method and return claims for further processing.
func (r simpleJWTSecured) Claims(ctx context.Context, secret string, method jwt.SigningMethod) (jwt.MapClaims, error) {
	value := r.jwtToken
	if v := ctx.Value(keys.JWT); v != nil {
		//checkin the correct types under the interface
		jwtKey, ok := v.(string)
		// if not a string
		if !ok {
			return nil, errors.New("claims context type assertion error, wrong type stored by JWTkey")
		}
		value = jwtKey
	}
	return parseJWTToken(value, secret, method)
}

// ValidateStandardJWTClaims is helper function for validation standard jwt payload.
func ValidateStandardJWTClaims(claims jwt.MapClaims) error {
	if _, ok := claims["exp"].(float64); !ok {
		return errors.New(errors.SecurityJWTInvalidEXPErrorMessage)
	}
	if _, ok := claims["iat"].(float64); !ok {
		return errors.New(errors.SecurityJWTInvalidIATErrorMessage)
	}
	if _, ok := claims["nbf"].(float64); !ok {
		return errors.New(errors.SecurityJWTInvalidNBFErrorMessage)
	}
	return nil
}

func parseJWTToken(jwtKey, secret string, method jwt.SigningMethod) (jwt.MapClaims, error) {
	if jwtKey == "" {
		return nil, errors.NewIncorrectAuthTokenError(errors.SecurityJWTEmptyKey)
	}
	parsedToken, err := jwt.Parse(jwtKey, func(token *jwt.Token) (interface{}, error) {
		// Don't forget to validate the alg is what you expect:
		if token.Method != method {
			return nil, errors.NewIncorrectAuthTokenError(fmt.Sprintf("unexpected signing method: %v", token.Header["alg"]))
		}
		return []byte(secret), nil
	})
	if err != nil {
		return nil, errors.NewIncorrectAuthTokenError(err.Error())
	}
	return parsedToken.Claims.(jwt.MapClaims), nil
}
