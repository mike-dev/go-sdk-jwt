package helpers

import (
	"encoding/json"
	"io/ioutil"
	"os"
	"path/filepath"

	"github.com/caarlos0/env"
)

// LoadSettingsFromFile loads settings from json file in config folder.
func LoadSettingsFromFile(configPath string, s interface{}) {

	afp, err := filepath.Abs(configPath)
	if err != nil {
		panic(err)
	}

	jsonConfig, err := os.Open(afp)
	if err != nil {
		panic(err)
	}
	defer func() {
		if err := jsonConfig.Close(); err != nil {
			panic(err)
		}
	}()

	data, err := ioutil.ReadAll(jsonConfig)
	if err != nil {
		panic(err)
	}

	err = json.Unmarshal(data, &s)
	if err != nil {
		panic(err)
	}
}

// LoadSettingsFromEnvironmentVariable loads settings from environment variable.
func LoadSettingsFromEnvironmentVariable(s interface{}) {
	if err := env.Parse(s); err != nil {
		panic(err)
	}
}
