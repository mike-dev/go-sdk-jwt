package helpers

import "github.com/leonelquinteros/gorand"

// VerifyUUID verifies uuid in text form.
func VerifyUUID(uuid string) bool {
	_, err := gorand.UnmarshalUUID(uuid)
	if err != nil {
		return false
	}
	return true
}
