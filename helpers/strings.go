package helpers

import (
	"fmt"
)

// InterfaceToString trying type assert to string, if not successful then return default format value.
func InterfaceToString(s interface{}) string {
	result, ok := s.(string)
	if ok {
		return result
	}
	return fmt.Sprintf("%+v", s)
}
