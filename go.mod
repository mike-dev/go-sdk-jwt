module bitbucket.org/mike-dev/go-sdk-jwt

go 1.13

require (
	github.com/caarlos0/env v3.5.0+incompatible // indirect
	github.com/dgrijalva/jwt-go v3.0.0+incompatible
	github.com/leonelquinteros/gorand v1.0.2
	github.com/openzipkin/zipkin-go v0.2.2
	github.com/pkg/errors v0.8.1
)
