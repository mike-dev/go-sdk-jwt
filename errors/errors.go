package errors

const (
	// UndefinedErrorCode is the error code used for undefined errors returned by microservices.
	UndefinedErrorCode = "UNDEF"
	// IncorrectAuthTokenErrorCode is the error code for incorrect auth token.
	IncorrectAuthTokenErrorCode = "AUTH001"
)

const (
	SecurityJWTInvalidEXPErrorMessage = "invalid exp"
	SecurityJWTInvalidIATErrorMessage = "invalid iat"
	SecurityJWTInvalidNBFErrorMessage = "invalid nbf"
	SecurityJWTEmptyKey               = "jwt token is empty"
)

// IncorrectAuthTokenError is the error for incorrectly formed authentication token.
type IncorrectAuthTokenError StandardError

// NewIncorrectAuthTokenError returns new IncorrectAuthTokenError instance.
func NewIncorrectAuthTokenError(reason string, keysAndValues ...interface{}) IncorrectAuthTokenError {
	return IncorrectAuthTokenError{
		SRCTError: SRCTError{
			"",
			reason,
			IncorrectAuthTokenErrorCode,
			New("traced: error").(StackTraced),
		},
		KeyValueSource: &KeyValueStore{KeysAndValuesToMap(keysAndValues)},
	}
}
