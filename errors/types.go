package errors

import (
	"fmt"

	"github.com/pkg/errors"
)

// SentryAware is used to identify do we need to send data to sentry or not.
type SentryAware interface {
	SendToSentry() bool
}

// KeyValueSource is the interface for injecting required k/v data.
type KeyValueSource interface {
	KeysValues() map[string]interface{}
}

// StackTraced is the interface for errors keeping the trace.
type StackTraced interface {
	StackTrace() errors.StackTrace
}

// Coded is an interface of something with the code.
type Coded interface {
	Code() string
}

// ErrorCoded implementation of error with code.
type ErrorCoded string

// Code is the function returning the error code.
func (ec ErrorCoded) Code() string {
	return string(ec)
}

// IgnoreSentry implements SentryAware interface.
type IgnoreSentry struct{}

// SendToSentry to define send to sentry or not.
func (i IgnoreSentry) SendToSentry() bool {
	return false
}

// SRCTError - SingleReasonCodedTracedError, the error that has a code, traced and
// the error message is the 'Prefix : Reason'.
type SRCTError struct {
	Prefix string
	Reason string
	ErrorCoded
	StackTraced
}

// Error returns the string representation of the error.
func (e SRCTError) Error() string {
	if e.Prefix == "" {
		return e.Reason
	}
	return fmt.Sprintf("%s: %s", e.Prefix, e.Reason)
}

// StandardError - the error type for all standard errors for our services.
// The functionality is enough in order to cover almost all existing errors.
type StandardError struct {
	SRCTError
	IgnoreSentry
	KeyValueSource
}

// StandardErrorAgreement - an interface for the standard error, what it should support
type StandardErrorAgreement interface {
	Coded
	StackTraced
	SentryAware
	KeyValueSource
}

var _ StandardErrorAgreement = StandardError{}

// ApplicationErrorCode returns application specified error code based on built-in error interface provided
func ApplicationErrorCode(err error) string {
	errCode := UndefinedErrorCode
	if eCoded, ok := err.(Coded); ok {
		errCode = eCoded.Code()
	}
	return errCode
}
