package errors

import (
	"fmt"

	"bitbucket.org/mike-dev/go-sdk-jwt/helpers"
	"github.com/pkg/errors"
)

const errorStructureField = "error_structure"

// ErrorStructureWrapper we need the structure in order to cheat zap logger lazy serialisation of errors from the key-values
// zapcore/field.go.AddTo(enc ObjectEncoder)
type ErrorStructureWrapper struct {
	ErrorItself error
}

// nonPrintedStructureError - if error supports the interface - we will not print its structure when wrap
type nonPrintedStructureError interface {
	KeyValueSource
}

// Wrap wrap error with prefix.
func Wrap(err error, prefix string) error {
	return Wrapkv(err, prefix)
}

// WrappedKeyValueError - the error that has a code, traced, key values source and
// the error message is the 'prefix : message'.
type WrappedKeyValueError StandardError

// KeyValueStore keep map with additional info.
type KeyValueStore struct {
	KeysAndValues map[string]interface{}
}

// KeysValues return keys values map.
func (k *KeyValueStore) KeysValues() map[string]interface{} {
	return k.KeysAndValues
}

// Error returns the string representation of the error.
func (e WrappedKeyValueError) Error() string {
	if e.Prefix == "" {
		return e.Reason
	}
	return fmt.Sprintf("%s : %s", e.Prefix, e.Reason)
}

// Wrapkv wrap error with prefix and save key values pairs,
// if last key don't have his value, it will be save with empty string.
func Wrapkv(err error, prefix string, keysAndValues ...interface{}) error {
	if nil == err {
		return nil
	}
	if e, ok := err.(*WrappedKeyValueError); ok {
		// add new key-values in the case when it is already a key-value error
		kvMap := e.KeysValues()
		for k, v := range KeysAndValuesToMap(keysAndValues) {
			kvMap[k] = v
		}
		e.Prefix = fmt.Sprintf("%s : %s", prefix, e.Prefix)
		return e
	}

	e := WrappedKeyValueError{}
	e.Prefix = prefix
	e.Reason = err.Error()

	if coded, ok := err.(Coded); ok {
		e.ErrorCoded = ErrorCoded(coded.Code())
	} else {
		e.ErrorCoded = UndefinedErrorCode
	}

	e.KeyValueSource = &KeyValueStore{KeysAndValues: KeysAndValuesToMap(keysAndValues)}

	if _, ok := err.(nonPrintedStructureError); !ok {
		e.KeyValueSource.KeysValues()[errorStructureField] = ErrorStructureWrapper{err}
	}

	if st, ok := err.(StackTraced); ok {
		e.StackTraced = st
	} else {
		e.StackTraced, ok = errors.Wrap(errors.New(""), "").(StackTraced)
		if !ok {
			panic("Can't type assertion to StackTraced")
		}
	}

	return &e
}

// KeysAndValuesToMap helper function to convert slice to map.
func KeysAndValuesToMap(keysAndValues []interface{}) map[string]interface{} {
	keysValues := make(map[string]interface{})
	for i := 0; i < len(keysAndValues); {
		if i == len(keysAndValues)-1 {
			keysValues[helpers.InterfaceToString(keysAndValues[i])] = ""
			break
		}
		k, v := keysAndValues[i], keysAndValues[i+1]
		keysValues[helpers.InterfaceToString(k)] = v
		i += 2
	}
	return keysValues
}

// New create new error by passed message.
func New(msg string) error {
	return Wrap(fmt.Errorf(msg), "")
}

// Newkv create new error by passed message and key values map.
func Newkv(msg string, keyValues ...interface{}) error {
	return Wrapkv(fmt.Errorf(msg), "", keyValues...)
}
